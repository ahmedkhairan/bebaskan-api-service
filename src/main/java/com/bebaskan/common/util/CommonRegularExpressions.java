package com.bebaskan.common.util;

import java.util.regex.Pattern;

public class CommonRegularExpressions {

    public static final Pattern MAIL_PATTERN = Pattern
    				//^([a-zA-Z0-9]+[_|\\_|\\.|\\-]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\\_|\\.]?)*[a-zA-Z0-9]+\\.[a-zA-Z]{2,4}$
            .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    public static final Pattern MOBILE_PATTERN = Pattern
            .compile("^[0-9]{8,12}$");
    public static final Pattern TEL_PHONE_PATTERN = Pattern
            .compile("^[0-9]{8,15}$");
    public static final Pattern SUB_MEMBER_PATTERN = Pattern
            .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*:[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*$");
    public static final Pattern NICK_MEMBER_PATTERN = Pattern
            .compile("^[a-zA-Z][a-zA-Z0-9_]*$");
    public static final Pattern FIRST_CHAR_NICK_MEMBER_PATTERN = Pattern
            .compile("^[a-zA-Z]");
    public static final Pattern PASSWORD_PATTERN = Pattern
            .compile("^[a-zA-Z]");

    public static final Pattern BANK_CARD = Pattern.compile("^\\d{1,16}$");

    public static final Pattern ZIP_CODE = Pattern.compile("^\\d{5}$");

    public static boolean match(String text, String type) {
        Pattern matcher = null;
        if ("zipCode".equals(type)) {
            matcher = ZIP_CODE;
        } else if ("mobilePhone".equals(type)) {
            matcher = MOBILE_PATTERN;
        } else if ("telPhone".equals(type)) {
            matcher = TEL_PHONE_PATTERN;
        } else if ("email".equals(type)) {
            matcher = MAIL_PATTERN;
        }
        return matcher.matcher(text).matches();
    }
}
