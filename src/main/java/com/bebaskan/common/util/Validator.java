package com.bebaskan.common.util;

import org.apache.commons.lang3.StringUtils;

import javax.imageio.ImageIO;
import java.io.InputStream;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Validator {

	private Validator(){}

	public static boolean isNullOrEmpty(Object value){
		if (null == value){
			return true;
		}

		if (value instanceof String){
			return value.toString().trim().length() <= 0;
		}

		if (value instanceof Collection){
			return ((Collection<?>) value).isEmpty();
		}

		if (value instanceof Map){
			return ((Map<?, ?>) value).isEmpty();
		}

		if (value instanceof Enumeration){
			return !((Enumeration<?>) value).hasMoreElements();
		}

		if (value instanceof Iterator){
			return !((Iterator<?>) value).hasNext();
		}

		boolean arrayFlag = arrayIsNullOrEmpty(value);
		if (arrayFlag){
			return true;
		}
		return false;
	}

	public static boolean isNotNullOrEmpty(Object value){
		return !isNullOrEmpty(value);
	}

	private static boolean arrayIsNullOrEmpty(Object value){
		if (value instanceof Object[]){
			return ((Object[]) value).length == 0;
		}

		if (value instanceof int[]){
			return ((int[]) value).length == 0;
		}

		if (value instanceof long[]){
			return ((long[]) value).length == 0;
		}

		if (value instanceof float[]){
			return ((float[]) value).length == 0;
		}

		if (value instanceof double[]){
			return ((double[]) value).length == 0;
		}

		if (value instanceof char[]){
			return ((char[]) value).length == 0;
		}

		if (value instanceof boolean[]){
			return ((boolean[]) value).length == 0;
		}

		if (value instanceof byte[]){
			return ((byte[]) value).length == 0;
		}

		if (value instanceof short[]){
			return ((short[]) value).length == 0;
		}
		return false;
	}
	
	public static boolean isValidImage(InputStream is) {
		try {
			if(ImageIO.read(is) != null){
				return true;				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean isContainHtml(String content) {
		String data = StringUtils.trimToEmpty(content);
		return 
				data.contains("<") || 
				data.contains(">") || 
				data.contains("&gt;") || 
				data.contains("&lt;");
	}	
	
	public static boolean isValidPassword(String password) {
		String regex = ValidationRegex.PASSWORD;

		Pattern p = Pattern.compile(regex);

		if (password == null) {
			return false;
		}

		Matcher m = p.matcher(password);

		return m.matches();
	}
	
	public static boolean isValidEmail(String email) {
		
		if (email == null || email.contains("+")) {
			return false;
		}
		
		String regex = "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

		Pattern p = Pattern.compile(regex);

		

		Matcher m = p.matcher(email);

		return m.matches();
	}
	
	public static boolean isValidPhoneNumber(String mobileNumber) {
		String regex = "^[0-9]{9,15}$";

		Pattern p = Pattern.compile(regex);

		if (mobileNumber == null) {
			return false;
		}

		Matcher m = p.matcher(mobileNumber);

		return m.matches();
	}

	public static boolean isAlphabetWithSpace(String text) {
		String regex = "^[a-zA-Z ]{3,50}$";

		Pattern p = Pattern.compile(regex);

		if (text == null) {
			return false;
		}

		Matcher m = p.matcher(text);

		return m.matches();
	}
	
	public static boolean validRegex(String value, String regex) {

		Pattern p = Pattern.compile(regex);

		if (value == null) {
			return false;
		}

		Matcher m = p.matcher(value);

		return m.matches();
	}

	public static boolean isValidUrl(String url) {
		if (url.matches("^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$")) {
			return true;
		} else return false;
	}

}
