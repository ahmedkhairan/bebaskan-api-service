
package com.bebaskan.common.util;

import java.math.BigDecimal;
import java.util.List;


public final class ObjectUtil {

	private ObjectUtil(){}

	public static boolean equalsNotNull(Object object,Object object2){
		if (Validator.isNotNullOrEmpty(object) && Validator.isNotNullOrEmpty(object2)){
			return object.equals(object2);
		}
		return false;
	}

	public static Boolean equals(Object object,Object object2,boolean flag_nullType){
		if (object == object2){
			return true;
		}
		if (null == object){
			if (flag_nullType){
				if ("".equals(object2.toString().trim())){
					return true;
				}
			}
		}else{
			if ("".equals(object.toString().trim())){
				if (null == object2){
					if (flag_nullType){
						return true;
					}
				}else{
					if ("".equals(object2.toString().trim())){
						return true;
					}
				}
			}else{
				return object.equals(object2);
			}
		}
		return false;
	}

	public static Boolean equals(Object object,Object object2){
		return equals(object, object2, false);
	}

	public static Boolean isBoolean(Object object){
		return object instanceof Boolean;
	}

	public static Boolean isInteger(final Object object){
		
		boolean isInteger = object instanceof Integer;
		
		if(!isInteger) {
			try{
				Integer.parseInt((String) object);
				isInteger = true;
			}catch(Exception e){
			}
		}
		
		return isInteger;
	}

	public static Boolean toBoolean(Object object){
		if (null == object){
			throw new IllegalArgumentException("object can't be null/empty!");
		}
		return Boolean.parseBoolean(object.toString());
	}

	public static Integer toInteger(Object value){
		Integer returnValue = null;
		if (Validator.isNotNullOrEmpty(value)){
			if (value instanceof Integer){
				returnValue = (Integer) value;
			}else{
				try{
					returnValue = new Integer(value.toString().trim());
				}catch (Exception e){
					throw new IllegalArgumentException("Input param:\"" + value + "\", convert to integer exception");
				}
			}
		}
		return returnValue;
	}

	public static BigDecimal toBigDecimal(Object value){
		BigDecimal returnValue = null;
		if (Validator.isNotNullOrEmpty(value)){
			if (value instanceof BigDecimal){
				returnValue = (BigDecimal) value;
			}else{
				returnValue = new BigDecimal(value.toString().trim());
			}
		}
		return returnValue;
	}

	public static Long toLong(Object value){
		if (Validator.isNotNullOrEmpty(value)){
			if (value instanceof Long){
				return (Long) value;
			}
			return Long.parseLong(value.toString());
		}
		return null;
	}

	public static Double toDouble(Object value){
		if (Validator.isNotNullOrEmpty(value)){
			if (value instanceof Double){
				return (Double) value;
			}
			return new Double(value.toString());
		}
		return null;
	}

	public static Float toFloat(Object value){
		if (Validator.isNotNullOrEmpty(value)){
			if (value instanceof Float){
				return (Float) value;
			}
			return new Float(value.toString());
		}
		return null;
	}

	public static Short toShort(Object value){
		if (Validator.isNotNullOrEmpty(value)){
			if (value instanceof Short){
				return (Short) value;
			}
			return new Short(value.toString());
		}
		return null;
	}

	public static String toString(Object value){
		if (null == value){
			return null;
		}
		if (value instanceof String){
			return (String) value;
		}
		return value.toString();
	}

	public static <T> T toT(Object value,Class<?> class1){
		if (null == value){
			return null;
		}
		if (class1 == String.class){
			return (T) toString(value);
		}else if (class1 == Boolean.class){
			return (T) toBoolean(value);
		}else if (class1 == Integer.class){
			return (T) toInteger(value);
		}else if (class1 == BigDecimal.class){
			return (T) toBigDecimal(value);
		}else if (class1 == Long.class){
			return (T) toLong(value);
		}else if (class1 == Double.class){
			return (T) toDouble(value);
		}else if (class1 == Float.class){
			return (T) toFloat(value);
		}else if (class1 == Short.class){
			return (T) toShort(value);
		}
		return null;
	}

	public static String trim(Object obj){
		return obj == null ? "" : obj.toString().trim();
	}
	
	public static Byte toByte(Object value){
		if (Validator.isNotNullOrEmpty(value)){
			if (value instanceof Byte){
				return (Byte) value;
			}
			return new Byte(value.toString());
		}
		return null;
	}
	
	public static List<Object> toList(Object value){
		if (Validator.isNotNullOrEmpty(value)){
			if (value instanceof List){
				return (List<Object>) value;
			}
			return null;
		}
		return null;
	}
	
	public static Long[] makeStringToLong(String splitedStrLong) {
		String[] longValue = null;
		if (splitedStrLong.indexOf(",") >= 0) {
			longValue = splitedStrLong.split(",");
		} else {
			longValue = new String[] { splitedStrLong };
		}
		Long[] arrLong = new Long[longValue.length];
		for (int i = 0; i < longValue.length; i++) {
			try {
				arrLong[i] = Long.parseLong(longValue[i].trim());
			} catch (Exception e) {
				arrLong[i] = null;
			}
		}
		return arrLong;
	}
	
	public static int indexOf(final Long[] source, final Long target) {
		int index = -1;
		
		if(Validator.isNullOrEmpty(source) || Validator.isNullOrEmpty(target)) {
			return index;
		}
		
		for (int i = 0; i < source.length; i++) {
			if(source[i].longValue() == target.longValue()) {
				return i;
			}
		}
		
		return index;
	}
}
