package com.bebaskan.common.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public final class StringUtil{

	private final static Logger	log	= LoggerFactory.getLogger(StringUtil.class);

	private StringUtil(){}

	public static boolean isContain(Object text,String beIncludedString){
		if (null == text){
			log.warn("the param \"text\" is null,default return false");
			return false;
		}
		int indexOf = text.toString().indexOf(beIncludedString);
		return indexOf != -1;
	}

	public static String substring(Object text, int beginIndex){
		String t = ObjectUtil.toString(text);
		if (Validator.isNullOrEmpty(t)){
			return null;
		}
		return t.substring(beginIndex);
	}

	public static String substring(Object textObject, int startIndex, int length){
		String returnValue = null;
		if (Validator.isNullOrEmpty(textObject)){
			return null;
		}
		String textString = ObjectUtil.toString(textObject);
		int textLength = textString.length();
		if (startIndex > textLength - 1){
			return null;
		}else if (startIndex == textLength - 1){
			return substringLast(textString, 1);
		}else if (length < 1){
			return null;
		}else if (1 == length){
			return textString.substring(startIndex, startIndex + 1);
		}else{
			int remainLength = textLength - startIndex;
			if (remainLength > length){
				int endIndex = startIndex + length;
				returnValue = textString.substring(startIndex, endIndex);
			}else{
				returnValue = textString.substring(startIndex);
			}
		}
		return returnValue;
	}

	public static String substring(String text, String beginString){
		return substring(text, beginString, 0);
	}

	public static String substring(String text, String beginString, int shift){
		if (Validator.isNullOrEmpty(text)){
			return null;
		}else if (Validator.isNullOrEmpty(beginString)){
			return null;
		}
		int beginIndex = text.indexOf(beginString);
			if (beginIndex == -1){
			return null;
		}
		int startIndex = beginIndex + shift;
		if (startIndex < 0){

			throw new IllegalArgumentException("startIndex < 0");
		}else if (startIndex > text.length()){
			return null;
		}
		return text.substring(startIndex);
	}

	public static String substring(String text,String startString,String endString){
		if (Validator.isNullOrEmpty(text)){
			return null;
		}else if (Validator.isNullOrEmpty(startString)){
			return text.substring(0, text.indexOf(endString));
		}
		int beginIndex = text.indexOf(startString);
		int endIndex = text.indexOf(endString);
		return text.substring(beginIndex, endIndex);
	}

	public static String getGetterMethodName(String fieldName){
		return "get" + StringUtil.firstCharToUpperCase(fieldName);
	}

	public static String firstCharToUpperCase(String word){
		return StringUtils.capitalize(word);
	}

	public static String firstCharToLowerCase(String word){
		return StringUtils.uncapitalize(word);
	}

	public static String substringLast(String text,int lastLenth){
		return text.substring(text.length() - lastLenth);
	}

	public static String substringWithoutLast(String text,int lastLenth){
		if (Validator.isNullOrEmpty(text)){
			return null;
		}
		return text.substring(0, text.length() - lastLenth);
	}

	public static String formatEmailOrPhone(String target){

		if(StringUtils.isBlank(target)){
			return null;
		}
		
		if (CommonRegularExpressions.MAIL_PATTERN.matcher(target).matches()){
			int index = target.lastIndexOf("@");
			String prefix = target.substring(0, index);
			String suffix = target.substring(index);
			if (prefix.length() == 1){
				prefix = prefix + "****";
			}else if (prefix.length() >= 2){
				prefix = prefix.toCharArray()[0] + "****" + prefix.toCharArray()[prefix.length() - 1];
			}

			return prefix + suffix;
		}else if (CommonRegularExpressions.MOBILE_PATTERN.matcher(target).matches()){
			target = "62"+target.substring(0, target.length() - 3) + "***";
			return target;

		}else{
			return null;
		}
	}

	public static String formatPhoneOTP(String target){
		if(StringUtils.isBlank(target)){
			return null;
		}

		if (CommonRegularExpressions.MOBILE_PATTERN.matcher(target).matches()){
			target = "0" + target.substring(0, 4) + "***" + target.substring(7, target.length());
			return target;
		}else{
			return null;
		}
	}

	public static byte[] toBytes(String value){
		return value.getBytes();
	}

	public static byte[] toBytes(String value,String charsetName){
		try{
			return value.getBytes(charsetName);
		}catch (UnsupportedEncodingException e){
			log.error("[FATAL]", e);
		}
		return null;
	}

	public static String format(String format,Object...args){
		return String.format(format, args);
	}

	public static String[] splitToStrings(String value,String spliter){
		if (null != value){
			String[] strings = value.split(spliter);
			return strings;
		}
		return null;
	}

	public static <T> T convertStringToT(String value,Class<?> class1){
		return (T) ObjectUtil.toT(value, class1);
	}

    public static String formatPathToUrl(String path,Object ... suffixs) {

        return StringUtil.formatPathToUrlAll(null, path, suffixs);
    }

    public static String formatPathToUrlAll(String prefix,String path,Object ... suffixs) {
        if (path == null) {
            throw new RuntimeException("Format path to url error,the url is null.");
        }

        String[] strings = path.split("/");

        int length = strings.length;
        if (length < 3) {
            throw new RuntimeException("Format path to url error,the url[" + path + "] is invalid.");
        }

        StringBuilder sBuilder = new StringBuilder();
        if (prefix != null) {
            sBuilder.append(prefix);
        }

        sBuilder.append("/");
        sBuilder.append(strings[length - 2]);
        sBuilder.append("/");
        sBuilder.append(strings[length - 1]);
        sBuilder.append("/");

        if(suffixs!=null&&suffixs.length!=0){
            for (int i = 0; i < suffixs.length; i++) {
                sBuilder.append(suffixs[i]);
            }
        }

        return sBuilder.toString();
    }

	public static boolean isStringNumericAndNotEmpty(String numericString){
		try {
			if(Validator.isNotNullOrEmpty(numericString) && StringUtils.isNumeric(numericString)){				
				return true;
			}
		} catch (Exception e) {
			log.error("[FATAL]", e);
		}
		return false;
	}

	public static boolean isStringAlphabeticOnly(String stringAlphabetic){
		Pattern alphabeticPattern = Pattern.compile("[a-zA-Z]+");
        if (((Matcher) alphabeticPattern.matcher(stringAlphabetic)).find()) {
        	return false;
        }
		return false;
	}

	public static String right(String str, int len) {
        if (str == null) {
            return null;
        }
        if (len < 0) {
            return "";
        }
        if (str.length() <= len) {
            return str;
        }
        return str.substring(str.length() - len);
    }

	public static String left(String str, int len) {
        if (str == null) {
            return null;
        }
        if (len < 0) {
            return "";
        }
        if (str.length() <= len) {
            return str;
        }
        return str.substring(0, len);
    }

    public static String normalizePhoneNumber(String phoneNumber){
    	if(Validator.isNullOrEmpty(phoneNumber)){
    		return phoneNumber;
    	}
    	phoneNumber = phoneNumber.trim();
        if("0".equals(StringUtil.left(phoneNumber, 1))){
            return StringUtil.right(phoneNumber, phoneNumber.length() - 1);
        } else if("+".equals(StringUtil.left(phoneNumber, 1))) {
            phoneNumber = StringUtil.right(phoneNumber, phoneNumber.length() - 1);
        }
        if("62".equals(StringUtil.left(phoneNumber, 2))){
        	phoneNumber = StringUtil.right(phoneNumber, phoneNumber.length() - 2);
        }
        return phoneNumber;
    }
    
	public static String covertObjectToString(Object o) {
		if(o instanceof String && !StringUtils.isEmpty(o.toString())) {
			return o.toString();
		}
		return null;
	}

	public static String leftPadding(String str, String padChar, int size) {
		if (Validator.isNullOrEmpty(padChar)) {
			return StringUtils.leftPad(str, size);
		}
		return StringUtils.leftPad(str, size, padChar);
	}

	public static String rightPadding(String str, String padChar, int size) {
		if (Validator.isNullOrEmpty(padChar)) {
			return StringUtils.rightPad(str, size);
		}
		return StringUtils.rightPad(str, size, padChar);
	}

	public static String getCurrency(long money) {
		DecimalFormat fmt = (DecimalFormat) NumberFormat.getInstance();
	    String symbol = Currency.getInstance(new Locale("id", "ID")).getSymbol(new Locale("id", "ID"));
	    fmt.setGroupingUsed(true);
	    fmt.setPositivePrefix(symbol + " ");
	    fmt.setNegativePrefix("-" + symbol + " ");
	    
	    return fmt.format(money).replace(",", ".");
	}
	
	public static String getUUID() {
		return UUID.randomUUID().toString();
	}

	public static String removeLeadingZeros(String str) {
		String regex = "^0+(?!$)";
		str = str.replaceAll(regex, "");
		return str;
	}

}