package com.bebaskan.common.util;

public class ValidationRegex {

	public static final String 	ANS = "^[a-zA-Z0-9 .//@_-]*$";
	public static final String 	AN = "^[a-zA-Z0-9]*$";
	public static final String 	A = "^[a-zA-Z]*$";
	public static final String 	N = "^[0-9]*$";
	public static final String 	MOBILE_NO = "^[0-9]{10,15}$";
	public static final String 	EMAIL = "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
	public static final String 	PASSWORD = "^(?=.*[0-9])(?=.*[a-zA-Z]).{6,9999}$";
	public static final String 	JWT = "^[A-Za-z0-9-_=]+\\.[A-Za-z0-9-_=]+\\.?[A-Za-z0-9-_.+/=]*$";
	public static final String IMAGE = "([^\\s]+(\\.(?i)(jpg|png))$)";
}
