package com.bebaskan.common.enumeration;

public enum MessageCodes {
	NO_PERMISSION_ACCESS("Maaf, anda tidak mempunyai akses untuk halaman ini."),
	SERVICE_UNAVAILABLE("Maaf, sistem sibuk. Mohon coba beberapa saat lagi."),
	SECRET_KEY_INVALID("Secret key is invalid"),
	REGISTRATION_USERNAME_EXIST("Username telah dipakai."),
	REGISTRATION_EMAIL_EXIST("Email telah dipakai"),
	;

	private final String value;
	
	MessageCodes(String value) {
		this.value = value;
	}
	
	public String value() {
		return this.value;
	}
}
