package com.bebaskan.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ErrorResponse {

    @JsonInclude(Include.NON_NULL)
    private String title;
    private String message;

    public ErrorResponse(String message) {
        super();
        this.message = message;
    }

    public ErrorResponse(String title, String message) {
        super();
        this.title = title;
        this.message = message;
    }

}
