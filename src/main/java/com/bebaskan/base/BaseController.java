package com.bebaskan.base;

import com.bebaskan.common.enumeration.MessageCodes;
import io.jsonwebtoken.SignatureException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
public class BaseController {

    @Autowired
    protected BaseMessage message;

    @Autowired
    protected ResponseBuilder responseBuilder;

    @ExceptionHandler({Exception.class})
    @ResponseBody
    public ResponseEntity<Object> handleException(Exception ex) {
        ErrorResponse response = null;

        try {
            if (ex instanceof BaseException) {

                BaseException be = (BaseException) ex;
                if (be.getArgs() != null) {
                    response = new ErrorResponse(message.getMessage(be.getCode(), be.getArgs()));
                } else {
                    response = new ErrorResponse(message.getMessage(be.getCode()));
                }

                return responseBuilder.build(response, be.getStatusCode());
            } else if (ex instanceof MethodArgumentNotValidException) {

                MethodArgumentNotValidException exc = (MethodArgumentNotValidException) ex;
                return responseBuilder.build(new ErrorResponse(exc.getBindingResult().getFieldError().getDefaultMessage()), HttpStatus.BAD_REQUEST);

            } else if (ex instanceof MissingServletRequestParameterException) {

                return responseBuilder.build(new ErrorResponse(((MissingServletRequestParameterException) ex).getLocalizedMessage()), HttpStatus.BAD_REQUEST);

            } else if (ex instanceof HttpMediaTypeNotSupportedException) {

                return responseBuilder.build(new ErrorResponse(((HttpMediaTypeNotSupportedException) ex).getLocalizedMessage()), HttpStatus.BAD_REQUEST);

            } else if (ex instanceof HttpMessageNotReadableException) {

                return responseBuilder.build(new ErrorResponse(((HttpMessageNotReadableException) ex).getLocalizedMessage()), HttpStatus.BAD_REQUEST);

            } else if (ex instanceof AccessDeniedException) {

                return responseBuilder.build(new ErrorResponse(message.getMessage(MessageCodes.NO_PERMISSION_ACCESS)), HttpStatus.UNAUTHORIZED);

            } else if (ex instanceof SignatureException) {

                return responseBuilder.build(new ErrorResponse(message.getMessage(MessageCodes.SECRET_KEY_INVALID)), HttpStatus.UNAUTHORIZED);

            } else if (ex instanceof BindException) {
                BindException exc = (BindException) ex;
                return responseBuilder.build(new ErrorResponse(exc.getBindingResult().getFieldError().getDefaultMessage()), HttpStatus.BAD_REQUEST);

            } else {
                log.error("Exception Happens. Cause :  ", ex);
                return responseBuilder.build(new ErrorResponse(message.getMessage(MessageCodes.SERVICE_UNAVAILABLE)), HttpStatus.SERVICE_UNAVAILABLE);
            }
        } catch (Exception e) {
            log.error("Exception Happens. Cause :  ", e);
            return responseBuilder.build(new ErrorResponse(message.getMessage(MessageCodes.SERVICE_UNAVAILABLE)), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
