package com.bebaskan.base;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Setter
@Getter
public class BaseResponse {

    private int code;
    private String status;
    private Object data;

    public BaseResponse(HttpStatus httpStatus) {
        super();
        this.code = httpStatus.value();
        this.status = httpStatus.getReasonPhrase();
    }

    public BaseResponse(Object data, HttpStatus httpStatus) {
        super();
        this.data = data;
        this.code = httpStatus.value();
        this.status = httpStatus.getReasonPhrase();
    }

}
