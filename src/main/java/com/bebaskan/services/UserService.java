package com.bebaskan.services;

import com.bebaskan.base.BaseException;
import com.bebaskan.common.enumeration.MessageCodes;
import com.bebaskan.common.util.Validator;
import com.bebaskan.dao.entities.ERole;
import com.bebaskan.dao.entities.Role;
import com.bebaskan.dao.entities.User;
import com.bebaskan.payload.request.SignupRequest;
import com.bebaskan.dao.repository.RoleRepository;
import com.bebaskan.dao.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder encoder;

    public void createUser(SignupRequest signUpRequest) throws BaseException {
        ValidateSignUpRequest(signUpRequest);
        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));
        Set<Role> roles = createRole(signUpRequest);
        user.setRoles(roles);
        userRepository.save(user);
    }

    private Set<Role> createRole(SignupRequest signUpRequest) {
        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (Validator.isNullOrEmpty(strRoles)) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    case "mod":
                        Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(modRole);

                        break;
                    default:
                        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }
        return roles;
    }

    private void ValidateSignUpRequest(SignupRequest signUpRequest) throws BaseException {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            throw new BaseException(MessageCodes.REGISTRATION_USERNAME_EXIST, HttpStatus.BAD_REQUEST);
        }
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            throw new BaseException(MessageCodes.REGISTRATION_EMAIL_EXIST, HttpStatus.BAD_REQUEST);
        }
    }
}
