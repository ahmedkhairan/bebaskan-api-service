# BEBASKAN Core API

[![BEBASKAN API](https://img.shields.io/badge/Pakaimuka%20Core-0.0.1-black.svg?style=plastic)](https://www.oracle.com/java/technologies/)
[![Java](https://img.shields.io/badge/Java-1.8.0-red.svg?style=plastic)](https://www.oracle.com/java/technologies/)
[![Maven](https://img.shields.io/badge/Maven-3.5.3-purple.svg?style=plastic)](https://maven.apache.org)
[![Spring Boot](https://img.shields.io/badge/Spring%20Boot-2.7.3.RELEASE-green.svg?style=plastic)](https://spring.io/projects/spring-boot)
[![Hibernate](https://img.shields.io/badge/Hibernate-5.2.17-yellowgreen.svg?style=plastic)](https://hibernate.org)
[![Docker](https://img.shields.io/badge/Docker-23.0.2-blue.svg?style=plastic)](https://docker.com)
[![Docker Compose](https://img.shields.io/badge/Docker%20Compose-1.29.2-yellow.svg?style=plastic)](https://docker.com)


please see the key feature below :

- Stateless Application
- Easy to Scale Up
- Docker Ready
- Sentralized Cache
- ...

## Feature
- Login
- Account Management
- Edit Password and Profile Picture
- ...


## Build & Deploy

```
Data Initialization :
---------------------
INSERT INTO roles(name) VALUES('ROLE_USER');
INSERT INTO roles(name) VALUES('ROLE_MODERATOR');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');

Maven Run :
-----------
1. Prepare Project
   $ git clone https://gitlab.com/ahmedkhairan/bebaskan-api-service.git
   $ cd base-api-service/

2. Build and Package the Project
   $ mvn -e clean spring-boot:run -Dactive.profile=local

Web Application URL :
http://localhost:8080

* PORT can be configured in application.properties
```

## Package Structure
**Controller > Service > Repository**

| Package Name                   | Usage                                                                                                                                                                          |
|--------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| com.bebaskan.controller       | REST API                                                                                                                                                                       |
| com.bebaskan.service          | Business logic & bridges to access repository                                                                                                                                  |
| com.bebaskan.dao.repositories | Spring data repository for accessing database (see [Spring Repositories](https://docs.spring.io/spring-data/data-commons/docs/1.6.1.RELEASE/reference/html/repositories.html)) |
| com.bebaskan.dao.entities     | Spring data JPA entity                                                                                                                                                         |
| com.bebaskan.security         | Security configuration                                                                                                                                                         |
| com.bebaskan.payload          | Request and response object                                                                                                                                                    |
| com.bebaskan.base             | Base class                                                                                                                                                                     |
| com.bebaskan.common           | Common library                                                                                                                                                                 |

## Contributor
| Name              | Role                    | Email                  |
|-------------------|-------------------------|------------------------|
| Ahmad Khairan     | Backend Dev             | ahmedkhaeran@gmail.com |
| Muhammad Andriyan | Infrastructure Engineer | andjoy@gmail.com       |
